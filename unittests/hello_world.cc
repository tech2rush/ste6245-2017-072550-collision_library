// gtest
#include <gtest/gtest.h> // googletest header file

// collision library
#include <collision_library.h>
using namespace collision;

// gmlib
#include <gmParametricsModule>
using namespace GMlib;

// stl
#include <memory>
#include <chrono>
#include <iostream>
using namespace std::chrono_literals;

/*
TEST(MyUniqueTestCategory, MyCategory_UniqueTestName_WhichPasses) {

  EXPECT_TRUE(true);
}

TEST(MyUniqueTestCategory, MyCategory_UniqueTestName_WhichFails) {

  EXPECT_FALSE(true);
}
*/

TEST(ControllerBasicTests, Simulatesphere_OneStep) {

    double dt = 0.1;
    Point<double,3> gold_pos{ 0.0, 0.21, 0.0 };

    Environment env;

    auto sphere = unittestDynamicPhysObjectFactory<GMlib::PSphere<float>>();
    sphere->environment = &env;
    sphere->velocity = Vector<double, 3>{0.0, 2.1, 0.0};
    sphere->curr_t_in_dt = seconds_type{0};

    auto controller = unittestCollisionControllerFactory();
    controller->add(sphere.get());

    Scene scene;
    scene.insert(controller.get());
    scene.prepare();

    scene.enabledFixedDt();
    scene.setFixedDt(dt);
    scene.start();
    scene.simulate();
    scene.prepare();


    auto sphere_pos = sphere->getGlobalPos();

    auto abs_tol = 1e-5;
    EXPECT_NEAR( sphere_pos(0), gold_pos(0), abs_tol );
    EXPECT_NEAR( sphere_pos(1), gold_pos(1), abs_tol );
    EXPECT_NEAR( sphere_pos(2), gold_pos(2), abs_tol );

    controller->remove(sphere.get());
    scene.remove(controller.get());
}

TEST (DSpherePlaneCollisionDetection, Collision)
{
    Environment env;

    auto sphere
        = unittestDynamicPhysObjectFactory<GMlib::PSphere<float>> (1.0f);
    sphere->environment = &env;
    sphere->velocity     = Vector<double, 3>{0.0, 9.1, 0.0};
    sphere->curr_t_in_dt = seconds_type{0};

    auto plane = unittestStaticPhysObjectFactory<GMlib::PPlane<float>> (
        Point<float, 3> (10.0f, 10.0f, 5.0f),
        Vector<float, 3> (-20.0f, 0.0f, 0.0f),
        Vector<float, 3> (0.0f, 0.0f, -5.0f));

    const seconds_type dt{1.0};
    const auto         collision = detectCollision (*sphere, *plane, dt);

    EXPECT_TRUE (collision.time > seconds_type{0.0} and collision.time <= dt);
}

TEST (DSpherePlaneImpactResponse, NewVelocity)
{

    auto sphere = unittestDynamicPhysObjectFactory<GMlib::PSphere<float>> ();
    sphere->velocity = Vector<double, 3>{0.0, 9.1, 0.0};

    auto plane = unittestStaticPhysObjectFactory<GMlib::PPlane<float>> (
        Point<float, 3> (10.0f, 10.0f, 5.0f),
        Vector<float, 3> (-20.0f, 0.0f, 0.0f),
        Vector<float, 3> (0.0f, 0.0f, -5.0f));

    const auto dt = seconds_type (1.0);
    computeImpactResponse (*sphere, *plane, dt);

    auto newVel = sphere->velocity;

    EXPECT_FLOAT_EQ (-9.1f, newVel[1]);
}
