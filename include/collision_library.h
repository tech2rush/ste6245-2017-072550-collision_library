#ifndef COLLISION_LIBRARY_H
#define COLLISION_LIBRARY_H

#define EPSILON 0.001
#define COR 0.8

// collision library interface
#include <collision_interface.h>
#include <deque>
#include <unordered_map>



namespace collision
{
    enum struct States
    {
        free,
        sliding,
        resting,
        noChange
    };

    struct StateChangeObject
    {
        DynamicPSphere* obj;
        std::unordered_set < StaticPPlane* > attachedPlanes{}; //Generalize?
        States state;
        seconds_type time;

        StateChangeObject( DynamicPSphere* o, std::unordered_set < StaticPPlane* > p, States s, seconds_type t )
            : obj{o}, attachedPlanes{p}, state{s}, time{t}{}
    };
/*
    class StillEnvironment : public Environment {
      GM_SCENEOBJECT(DefaultEnvironment)
    public:
        GMlib::Vector<double, 3> externalForces () const override {
          return GMlib::Vector<double, 3> {0.0f, 0.0f, 0.0f};
      }
    };
    */

    template <>
    class DynamicPhysObject<GMlib::PSphere<float>> : public DynamicPhysObject_Base<GMlib::PSphere<float>> {
    public:
        using DynamicPhysObject_Base<GMlib::PSphere<float>>::DynamicPhysObject_Base;

        States state = States::free;

        void    simulateToTInDt( seconds_type t ) override;

        GMlib::Vector<double, 3>
        computeTrajectory (seconds_type dt) const override; // [m]

        GMlib::Vector<double, 3> adjustTrajectory (seconds_type dt) const;

        GMlib::Vector<double, 3> externalForces () const override; // [m / s^2]
    };


    class MyController : public Controller {
        GM_SCENEOBJECT(MyController)
    public:
        MyController();
        void add (DynamicPSphere* const sphere);
        void add (StaticPSphere* const sphere);
        void add (StaticPPlane* const plane);
        void add (StaticPCylinder* const cylinder);
        void add (StaticPBezierSurf* const surf);

        std::unordered_set< StaticPPlane* > getAttachedPlanes(DynamicPSphere* sphere);
        void setAttachedPlane(std::unordered_set<StaticPPlane *> sphere, DynamicPSphere*plane);

        void detachObjects( DynamicPSphere* sphere ); //Flytt til DynamicPSphere?

        void detectStateChanges( double dt );
        StateChangeObject detectStateChange(DynamicPSphere* sphere, double dt );

        void handleStates(StateChangeObject& so, double dt );

        void detectCollision( double dt );
        void handleCollision(const CollisionObject &col, double dt);

        std::shared_ptr< collision::Environment> env;

        Environment stillEnvironment;

       // void singularityCheck(double dt);

    protected:
        void localSimulate(double dt) override;

        std::vector<DynamicPSphere*>    _dynamic_spheres;
        std::vector<StaticPSphere*>     _static_spheres;
        std::vector<StaticPPlane*>      _static_planes;
        std::vector<StaticPCylinder*>   _static_cylinders;
        std::vector<StaticPBezierSurf*> _static_bezier_surf;

        std::deque<collision::CollisionObject > _collisions;
        std::deque< StateChangeObject > _singularities;

        std::unordered_map< DynamicPSphere*, std::unordered_set< StaticPPlane* > > _map;
    };


    template <class Container_T >
    void sortAndMakeUnique( Container_T& container)
    {
        auto pred = []( const auto& col1, const auto& col2 )
        {
            auto is_dynamic = []( const auto* obj )
            {
                if(dynamic_cast<const DynamicPSphere *>(obj)) return true;
                return false;
            };

            if(col1.obj1 == col2.obj1) return true;
            if(col1.obj1 == col2.obj2 ) return true;
            if (col1.obj2 == col2.obj1 ) return true;
            if(( is_dynamic(col1.obj2) or is_dynamic(col2.obj2)) and col1.obj2 == col2.obj2) return true;
            return false;
        };
        //Sort
        std::sort(container.begin(), container.end(),
                  [](const auto& obj1, const auto& obj2)
        {return (obj1.t_in_dt < obj2.t_in_dt);}
        );
        //Make unique
        auto new_end = container.end();

        for (auto itr1 = container.begin(); itr1 != new_end; ++itr1)
        {
            for(auto itr2 = new_end - 1; itr2 != itr1; --itr2)
            {
                if (pred(*itr1,*itr2)) {
                    std::swap(*itr2, *(new_end-1));
                    --new_end;
                }
            }
        }
        //Cleanup / remove dupes
        container.erase(new_end, end(container));
    }

    template <class Container_T >
    void sortAndMakeUniqueStates( Container_T& container)
    {
        auto pred = []( const auto& state1, const auto& state2 )
        {
            if (state1.obj == state2.obj) return true;
            return false;
        };
        //Sort
        std::sort(container.begin(), container.end(), [](const auto& obj1, const auto& obj2)
        {
            return (obj1.time < obj2.time);
        }
        );
        //Make unique
        auto new_end = container.end();

        for (auto itr1 = container.begin(); itr1 != new_end; ++itr1)
        {
            for(auto itr2 = new_end - 1; itr2 != itr1; --itr2)
            {
                if (pred(*itr1,*itr2)) {
                    std::swap(*itr2, *(new_end-1));
                    --new_end;
                }
            }
        }
        //Cleanup / remove dupes
        container.erase(new_end, end(container));
    }

    //Bjørn-Richard Pedersen
template <typename Container_1, typename Container_2>
void sortAndMakeBothUnique(Container_1 ColContainer, Container_2 stateContainer) {

    std::deque<collision::CollisionObject>         _newCollisions;
    std::deque<collision::StateChangeObject>          _newStateOjects;

    auto amIinCollision = [](Container_1 a, const auto& b){
        for(auto& c : a) {
            if(c.obj1==b.obj1) return true;
            if(c.obj1==b.obj2) return true;
            if(c.obj2==b.obj1) return true;
            if(c.obj2==b.obj2) return true;
            return false;
        }

    };

    auto amIinState = [](Container_2 a, const auto& b){
        for(auto& d:a){
            if(d.obj==b.obj) return true;
            return false;
        }
    };

    auto objPred = [](const auto& a, const auto& b) {
        if(a.obj1 == b.obj or a.obj2 == b.obj ) return true;
        return false;
    };

    auto timePred = [](const auto&a, const auto&b) {
        if(a.t_in_dt < b.time ) return true;
        return false;
    };

    bool colBigger;

    if( ColContainer.size() > stateContainer.size() ) {

       colBigger = true;

    }
    else colBigger = false;

    if( colBigger == true ) {

        bool placed = false;

        for(auto firstIter = std::end(ColContainer) - 1; firstIter!= std::begin(ColContainer) - 1;--firstIter)
        {
            for(auto secondIter = std::end(stateContainer) - 1; secondIter!= std::begin(stateContainer) - 1;--secondIter)
            {
                placed = false;

                //Check for same Objects
                if(objPred(*firstIter,*secondIter))
                {
                    //check for time of objects in both container
                    if(timePred(*firstIter,*secondIter))
                    {
                        //check if already in new container
                        if(amIinCollision(_newCollisions,*firstIter)==false) {
                            _newCollisions.push_back(*firstIter);
                            placed = true;
                        }
                    }

                    else
                    {
                        //check if already in new container
                        if(amIinState(_newStateOjects,*secondIter)==false)
                        {
                            _newStateOjects.push_back(*secondIter);
                            placed = true;
                        }
                    }
                }
            }

            // If col object NOT in states
            if (placed == false) _newCollisions.push_back(*firstIter);

        }

        for( auto& state : stateContainer) {

            if(amIinState(_newStateOjects, state) == false ) _newStateOjects.push_back(state);
        }
    }

    // States are bigger
    else {

        bool placed = false;

        for(auto firstIter = std::end(stateContainer) - 1; firstIter!= std::begin(stateContainer) - 1;--firstIter)
        {
            for(auto secondIter = std::end(ColContainer) - 1; secondIter!= std::begin(ColContainer) - 1;--secondIter)
            {

                placed = false;

                //Check for same Objects
                if(objPred(*secondIter,*firstIter))
                {
                    //check for time of objects in both container
                    if(timePred(*secondIter,*firstIter))
                    {
                        //check if already in new container
                        if(amIinCollision(_newCollisions,*secondIter)==false) {
                            _newCollisions.push_back(*secondIter);
                            placed = true;
                        }
                    }

                    else
                    {
                        //check if already in new container
                        if(amIinState(_newStateOjects,*firstIter)==false)
                        {
                            _newStateOjects.push_back(*firstIter);
                            placed = true;
                        }
                    }
                }
            }

            // If col object NOT in states
            _newStateOjects.push_back(*firstIter);
        }

        for( auto& collision : ColContainer) {

            if(amIinCollision(_newCollisions, collision) == false ) _newCollisions.push_back(collision);
        }

    }
    }



    template <class PSurf_T, typename... Arguments>
    std::unique_ptr<DynamicPhysObject<PSurf_T>> unittestDynamicPhysObjectFactory(Arguments... parameters)
    {
        return std::make_unique<DynamicPhysObject<PSurf_T>>(parameters...);
    }

    template <class PSurf_T, typename... Arguments>
    std::unique_ptr<StaticPhysObject<PSurf_T>> unittestStaticPhysObjectFactory(Arguments... parameters)
    {
        return std::make_unique<StaticPhysObject<PSurf_T>>(parameters...);
    }


} // END namespace collision



#endif //COLLISION_LIBRARY_H
