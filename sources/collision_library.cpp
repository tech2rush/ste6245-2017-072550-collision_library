#include "../include/collision_library.h"

#include <stdlib.h>
#include <math.h>



namespace collision
{


CollisionState
detectCollision (const DynamicPhysObject<GMlib::PSphere<float>>& S0,
                 const DynamicPhysObject<GMlib::PSphere<float>>& S1,
                 seconds_type                                    dt)
{
    //Velocities
    auto curr_t_in_dt = S0.curr_t_in_dt > S1.curr_t_in_dt ? S0.curr_t_in_dt : S1.curr_t_in_dt;
    const auto new_dt = dt - curr_t_in_dt;
    const auto ds0 = S0.computeTrajectory(new_dt);
    const auto ds1 = S1.computeTrajectory(new_dt);

    //Positions
    const auto p0 = S0.getMatrixToScene() * S0.getPos();
    const auto p1 = S1.getMatrixToScene() * S1.getPos();
    const auto r = S0.getRadius() + S1.getRadius();

    const auto Q = p1 - p0;
    const auto R = ds1 - ds0;

    const auto a = R * R;
    const auto b = 2 * ( Q * R );
    const auto c = Q * Q - r * r;
    const auto radicand = b * b - 4 * a * c;

    //*** Check for singularities ***
    if (c < EPSILON) return CollisionState(
                seconds_type{0.0},
                CollisionStateFlag::SingularityParallelAndTouching);

    if (a < EPSILON) return CollisionState(
                seconds_type{0.0},
                CollisionStateFlag::SingularityParallel);

    if ((radicand) < 0) return CollisionState(
                seconds_type{0.0},
                CollisionStateFlag::SingularityNoCollision);

    //Calculate time to impact, pick smallest.
    const auto x1 = ( -b + sqrt(radicand) ) / ( 2 * a );
    const auto x2 = ( -b - sqrt(radicand) ) / ( 2 * a );
    const auto x = x1 < x2 ? x1 : x2;


    if ( 0 < x <= 1 ) return CollisionState(
                seconds_type{x * dt.count()} +  curr_t_in_dt,
                CollisionStateFlag::Collision);
    /* We should never reach this point unless there's something wrong above.
        return CollisionState(
                    seconds_type{x},
                    CollisionStateFlag::SingularityNoCollision);
        */
}

CollisionState
detectCollision (const DynamicPhysObject<GMlib::PSphere<float>>& S0,
                 const StaticPhysObject<GMlib::PSphere<float>>&  S1,
                 seconds_type                                    dt)
{
}

CollisionState
detectCollision (const DynamicPhysObject<GMlib::PSphere<float>>& S,
                 const StaticPhysObject<GMlib::PPlane<float>>&   P,
                 seconds_type                                    dt)
{
    //Find plane normal
    auto M = const_cast< StaticPhysObject<GMlib::PPlane<float>>&> (P).evaluateParent(0.5f, 0.5f, 1, 1);
    GMlib::Vector<float,3> n = M(1)(0) ^ M(0)(1);
    n.normalize();

    //Speed perpendicular of plane

    GMlib::Vector<double, 3> ds;
    //if (S.state == DynamicPSphere::States::sliding) ds = S.adjustTrajectory(dt);
    //else
    ds = S.computeTrajectory(dt);

    const auto R = (ds * n);

    //Distance between sphere surface and plane
    const auto q = P.getMatrixToScene() * M(0)(0);
    const auto p = S.getMatrixToScene() * S.getPos();
    const auto d = (q + S.getRadius() * n) - p;
    const auto Q = (d * n);//Not using vector form

    if (abs(R) < EPSILON)
    {
        //Check for virtually parallell movement with intersection
        if ( abs(Q) < EPSILON)
        {
            return CollisionState(seconds_type{0.0}, CollisionStateFlag::SingularityParallelAndTouching);
        }
        return CollisionState(seconds_type{0.0}, CollisionStateFlag::SingularityParallel);
    }
    //Calculate impact time.
    const auto x = Q / R;

    //Check if impact is within current timeframe.
    //and that we are in fact moving towards the plane.
    if (0 < x <= 1 and R < 0)
    {
        return CollisionState(seconds_type {x * dt.count()} + S.curr_t_in_dt , CollisionStateFlag::Collision);
    }
    return CollisionState(seconds_type{0.0}, CollisionStateFlag::SingularityNoCollision);
}

CollisionState
detectCollision (const DynamicPhysObject<GMlib::PSphere<float>>&  S,
                 const StaticPhysObject<GMlib::PCylinder<float>>& C,
                 seconds_type                                    dt)
{

}

CollisionState
detectCollision (const DynamicPhysObject<GMlib::PSphere<float>>&  S,
                 const StaticPhysObject<GMlib::PBezierSurf<float>>& B,
                 seconds_type                                    dt)
{

}



void
computeImpactResponse (DynamicPhysObject<GMlib::PSphere<float>>& S0,
                       DynamicPhysObject<GMlib::PSphere<float>>& S1,
                       seconds_type                              dt)
{
    GMlib::Vector<double,3> d = S1.getGlobalPos().toType<double>() - S0.getGlobalPos().toType<double>();
    d.normalize();

    const auto v0_d = S0.velocity * d;
    const auto v1_d = S1.velocity * d;

    const auto m0 = S0.mass;
    const auto m1 = S1.mass;

    const auto vv0_d = v0_d * (m0 - m1) / (m0 + m1) + v1_d * 2 * m1 / (m0 + m1);
    const auto vv1_d = v1_d * (m1 - m0) / (m0 + m1) + v0_d * 2 * m0 / (m0 + m1);
    /*
        S0.velocity += (vv0_d - v0_d) * d;
        S1.velocity += (vv1_d - v1_d) * d;
        */
    //Below gives same result as above. Unnecessary calculations?
    const auto n = (GMlib::Vector<double,3>(d).getLinIndVec()).getNormalized();
    const auto v0_n = S0.velocity * n;
    const auto v1_n = S1.velocity * n;



    S0.velocity = v0_n * n + vv0_d *d * COR;
    S1.velocity = v1_n * n + vv1_d *d * COR;
}

void
computeImpactResponse (DynamicPhysObject<GMlib::PSphere<float>>& S0,
                       const StaticPhysObject<GMlib::PSphere<float>>&  S1,
                       seconds_type                              dt)
{


}

void
computeImpactResponse (DynamicPhysObject<GMlib::PSphere<float>>& S,
                       const StaticPhysObject<GMlib::PPlane<float>>&   P,
                       seconds_type                              dt)
{
    const auto p_n = P.getNormal();
    const auto v_n = S.velocity * p_n;

    //std::cout << "p_n " << p_n << " S.velocity (old) " << S.velocity <<
    //             " v_n " << v_n << std::endl;

    S.velocity -= (1 + COR) * v_n * p_n;

    //std::cout << "p_n " << p_n << " S.velocity (new) " << S.velocity << std::endl;
}

void
computeImpactResponse (DynamicPhysObject<GMlib::PSphere<float>>&  S,
                       const StaticPhysObject<GMlib::PCylinder<float>>& C,
                       seconds_type                              dt)
{
}

void
computeImpactResponse (DynamicPhysObject<GMlib::PSphere<float>>&  S,
                       const StaticPhysObject<GMlib::PBezierSurf<float>>& B,
                       seconds_type                              dt)
{
}


void
DynamicPhysObject<GMlib::PSphere<float> >::simulateToTInDt(seconds_type t)
{
    //Prepp
    auto dt = t - curr_t_in_dt;
    auto MI = getMatrixToSceneInverse();
    //Move
    auto ds = computeTrajectory(dt);
    translateParent(MI * ds);
    curr_t_in_dt = t;
    velocity = ds / dt.count();
}

GMlib::Vector<double,3>
DynamicPhysObject<GMlib::PSphere<float> >::computeTrajectory(seconds_type dt_in) const
{
    auto dt = dt_in.count();
    auto F = environment->externalForces();
    auto a = 0.5 * F * mass * dt * dt;
    return velocity * dt + a;
}

GMlib::Vector<double, 3>
DynamicPhysObject<GMlib::PSphere<float> >::adjustTrajectory(seconds_type dt) const
{
    const auto ds = computeTrajectory(dt);
    const auto r = getRadius();
    const auto s = getGlobalPos();
    const auto p = ds + s;

}



GMlib::Vector<double,3>
DynamicPhysObject<GMlib::PSphere<float> >::externalForces() const
{

    assert(environment != nullptr);
    return environment->externalForces();
}

MyController::MyController()
{
    env = std::make_shared< collision::DefaultEnvironment >();
}

void MyController::add(DynamicPSphere * const sphere)
{
    sphere->environment = new collision::DefaultEnvironment();
    _dynamic_spheres.push_back(sphere);
}

void MyController::add(StaticPSphere * const sphere) { _static_spheres.push_back(sphere); }

void MyController::add(StaticPPlane * const plane) { _static_planes.push_back(plane); }

void MyController::add(StaticPCylinder * const cylinder) { _static_cylinders.push_back(cylinder); }

void MyController::add(StaticPBezierSurf * const surf) { _static_bezier_surf.push_back(surf); }


//Copied from Bjørn-Richard Pedersen
std::unordered_set<StaticPPlane *> MyController::getAttachedPlanes(DynamicPSphere *sphere)
{
    return (_map[sphere]);
}

//Copied from Bjørn-Richard Pedersen
void MyController::setAttachedPlane(std::unordered_set< StaticPPlane *> plane, DynamicPSphere *sphere)
{
    for( auto& p : plane) {
        _map[sphere].emplace(p);
    }
}

//Copied from Bjørn-Richard Pedersen
void MyController::detachObjects(DynamicPSphere *sphere)
{
    _map.erase(sphere);
}

void MyController::detectStateChanges(double dt)
{
    for( auto& sphere : _dynamic_spheres)
    {
        auto singularity = detectStateChange(sphere, dt);

        //Main part
        if (singularity.state != sphere->state)
        {
            std::cout << "Adding singularity of type: " << int(singularity.state) << std::endl;
            _singularities.emplace_back(singularity);
        }
    }
}

//Code "inspired" by Bjørn-Richard Pedersen (used as reference).
StateChangeObject MyController::detectStateChange(DynamicPSphere *sphere, double dt)
{
    std::unordered_set< StaticPPlane* > pContainer;
    States state;

    const auto r = sphere->getRadius();
    const auto p = sphere->getGlobalPos();

    const auto time = sphere->curr_t_in_dt;
    const auto new_dt = seconds_type{dt} - time;

    const auto ds = sphere->computeTrajectory(new_dt);

    const auto planes = getAttachedPlanes(sphere);

    if ( planes.empty() )
    {
        for ( auto &plane: _static_planes )
        {
            const auto M = plane->evaluateParent(0.5f, 0.5f, 1, 1);
            const auto q = M(0)(0);
            GMlib::Vector<float,3> n = M(1)(0) ^ M(0)(1);
            n.normalize();
            const auto d = q - p + r * n;

            //Reference compendium, chapter 2.2 and appendix E
            const auto foo = std::abs(((-n*r) * ds) -(ds*ds));
            const auto dsn = ds * n; //speed normal to plane
            const auto dn = d * n; //distance normal to plane

            //Not sure about validity of this.
            //Can sphere ever be resting without triggering sliding conditions as they stand?
            state = States::free;

            if ( std::abs(dn) < EPSILON and dsn <= EPSILON )
            {
                /*
                if ( foo < EPSILON)
                {
            std::cout << "Request resting state" << std::endl;
            state = States::resting;
                }
                else
                {
                */
                std::cout << "Request sliding state" << std::endl;
                state = States::sliding;
                //}
            }
            else if ( std::abs(dn) < EPSILON and foo < EPSILON )
            {
                std::cout << "Request resting state" << std::endl;
                state = States::resting;
            }

            if (state == States::sliding or state == States::resting)
            {
                std::cout << "Attaching to plane with state: " << int(state) << std::endl;
                pContainer.insert(plane);
            }
            return StateChangeObject( sphere, pContainer, state, time);
        }
    }
    else
    {
        GMlib::APoint<float,3> q;
        GMlib::Vector<float,3> n {0.0f, 0.0f, 0.0f};

        for( auto &plane : planes)
        {
            const auto M = plane->evaluateParent(0.5f, 0.5f, 1, 1);
            GMlib::Vector<float,3> normal = M(1)(0) ^ M(0)(1);
            n += normal;
        }
        n.normalize();
        const auto d = q - p + n * r;

        const auto foo = std::abs(((-n*r) * ds) -(ds*ds));
        const auto dsn = ds * n; //speed normal to plane
        const auto dn = d * n; //distance normal to plane

        switch(sphere->state)
        {
        case States::sliding :
            if ( std::abs(dn) > EPSILON and dsn > EPSILON )
            {
                std::cout << "Switching state to free for sphere: " << &sphere << std::endl;
                state = States::free;
                const auto x = dn / dsn;
                return StateChangeObject( sphere, planes, state, (x * new_dt) + time);
            }
            else if ( foo < EPSILON )
            {
                std::cout << "Switching state to resting for sphere: " << &sphere << std::endl;
                state = States::resting;
                const auto x = dn / dsn;
                return StateChangeObject( sphere, planes, state, (x * new_dt) + time);
            }
            else return StateChangeObject( sphere, planes, States::sliding, time);
            break;

        case States::resting :
            if( foo > EPSILON )
            {
                std::cout << "Switching state to sliding for sphere: " << &sphere << std::endl;
                state = States::sliding;
                return StateChangeObject(sphere, planes, state, time);
            }
            else if( dsn > EPSILON ) {
                state = States::free;
                std::cout << "Switching state to free for sphere: " << &sphere << std::endl;
                return StateChangeObject(sphere, planes, state, time);
            }
            else return StateChangeObject(sphere, planes, States::resting, time);
            break;
        }
    }
}


void
MyController::localSimulate(double dt)
{
    for (auto d : _dynamic_spheres) d->curr_t_in_dt = seconds_type{0};

    detectStateChanges( dt );
    auto size =_singularities.size();
    if ( size > 0) std::cout << "Number of singularities before sort/unique " << size << std::endl;
    sortAndMakeUniqueStates( _singularities );
    if ( size > 0) std::cout << "Number after sort/unique " << _singularities.size() << std::endl;

    if (_singularities.size() > 0) std::cout << "Number of singularities before sort/unique " << _singularities.size() << std::endl;

    detectCollision( dt );
    sortAndMakeUnique( _collisions );

    if ( !_collisions.empty() and !_singularities.empty() )
        sortAndMakeBothUnique(_collisions, _singularities);

    while ( !_collisions.empty() or !_singularities.empty() )
    {
        if (!_collisions.empty() and !_singularities.empty() )
        {
            //Collision happens first
            if (_collisions.front().t_in_dt < _singularities.front().time)
            {
                std::cout << "Handling collision, singularities pending." << std::endl;
                auto col = _collisions.front();
                handleCollision(col, dt);
                _collisions.pop_front();
           }
            else
            {
                std::cout << "Handling singularity, collisions pending." << std::endl;
                auto sing = _singularities.front();
                handleStates(sing, dt);
                _singularities.pop_front();
            }
       }
        else if (!_collisions.empty() and _singularities.empty() )
        {
                std::cout << "Handling collision." << std::endl;
                auto col = _collisions.front();
                handleCollision(col, dt);
                _collisions.pop_front();
        }
        else if (_collisions.empty() and !_singularities.empty() )
        {
                std::cout << "Handling singularity." << std::endl;
                auto sing = _singularities.front();
                handleStates(sing, dt);
                _singularities.pop_front();
        }
        detectCollision(dt);
        detectStateChanges(dt);
        sortAndMakeUnique(_collisions);
        sortAndMakeUniqueStates(_singularities);
        if ( !_collisions.empty() and !_singularities.empty() )
            sortAndMakeBothUnique(_collisions, _singularities);
    }
    for (auto d : _dynamic_spheres) d->simulateToTInDt(seconds_type{dt});
}

void MyController::handleStates(StateChangeObject &so, double dt)
{
    std::cout << "Handle state: " << int (so.state) << std::endl;

    if ( so.state == States::free )
    {
        std::cout << "Detaching objects " << std::endl;
        detachObjects( so.obj);
        so.obj->state = so.state;
    }
    else
    {
        setAttachedPlane( so.attachedPlanes, so.obj);
        so.obj->state = so.state;
        std::cout << "Setting state : " << std::endl;

        if ( so.obj->state == States::resting )
        {
            std::cout << "Putting object into resting state " << std::endl;
            so.obj->velocity = {0.0f, 0.0f, 0.0f};
            so.obj->curr_t_in_dt = so.time;
            so.obj->environment = &stillEnvironment;
        }
        else so.obj->simulateToTInDt(seconds_type{dt});
    }
}

void MyController::detectCollision(double dt)
{
    for( auto s1: _dynamic_spheres)
    {
        for( auto s2: _dynamic_spheres)
        {
            auto col = collision::detectCollision(*s1, *s2, seconds_type{dt});
            auto min_curr_t_in_dt = s1->curr_t_in_dt > s2->curr_t_in_dt ? s1->curr_t_in_dt : s2->curr_t_in_dt;
            if ( col.flag == collision::CollisionStateFlag::Collision
                 and col.time.count() <= dt
                 and col.time >= min_curr_t_in_dt)
                _collisions.emplace_back(collision::CollisionObject(s1, s2, col.time));
        }

        for ( auto p: _static_planes)
        {
            auto col = collision::detectCollision(*s1, *p, seconds_type{dt});
            if ( col.flag == collision::CollisionStateFlag::Collision
                 and col.time.count() < dt
                 and col.time > s1->curr_t_in_dt)
                _collisions.emplace_back(collision::CollisionObject(s1, p, col.time));
        }
    }
}

void MyController::handleCollision(const CollisionObject &col, double dt)
{
        std::cout << "Collision_count " << _collisions.size() << std::endl;
        //const auto& col = _collisions.front();
        col.obj1->simulateToTInDt( col.t_in_dt );
        col.obj2->simulateToTInDt( col.t_in_dt );

        //Only dynamic object is sphere for now.
        const auto obj1 = dynamic_cast<collision::DynamicPSphere* >(col.obj1);

        //Check type by detecting failed dynamic_casts.
        //Note this should be rewritten to avoid casting twice (if successfull)
        //But this is postponed b/c of deadline.

        bool dynamic_cast_succeeded = false;
        //Planes
        if( dynamic_cast<collision::StaticPPlane* >(col.obj2) != nullptr)
        {
            dynamic_cast_succeeded = true;
            const auto obj2 = dynamic_cast<collision::StaticPPlane* >(col.obj2);
            std::cout << "Computing response against Static Plane: " <<
                         col.obj2 << std::endl;
            computeImpactResponse(*obj1, *obj2, col.t_in_dt);
        }
        else if( dynamic_cast<collision::DynamicPSphere* >(col.obj2) != nullptr)
        {
            std::cout << "Computing response against Dynamic Sphere: " <<
                         col.obj2 << std::endl;
            dynamic_cast_succeeded = true;
            const auto obj2 = dynamic_cast<collision::DynamicPSphere* >(col.obj2);
            computeImpactResponse(*obj1, *obj2, col.t_in_dt);
        }

        assert(dynamic_cast_succeeded = true);
}



std::unique_ptr<Controller> unittestCollisionControllerFactory() {

    return std::make_unique<MyController>();
}

} // END namespace collision

